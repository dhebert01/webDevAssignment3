'use strict';

/**
 * description here
 * @author your name
 * David Hebert
 */
document.addEventListener("DOMContentLoaded", setup)

function setup(e){
  const btn = document.getElementById("table");
  const row = document.getElementById("row");
  const column = document.getElementById("column");
  const colour1 = document.getElementById("colour1");
  const colour2 = document.getElementById("colour2");
  const table = document.getElementsByTagName("table")[0];

  function createTable(inputRow, inputColumn, inputColour1, inputColour2){
    let numColmunn = 0;
    let colour = 0;
    for(let i = 0; i < inputRow; i++){
      const createRow = document.createElement("tr");
      table.appendChild(createRow);
      colour = 0;
      if(i%2 === 0){
        colour = 1;
      }
      for(let j = 0; j < inputColumn; j++){
        //creates the table
        const row = document.getElementsByTagName("tr")[i];
        const createColumn = document.createElement("td");
        row.appendChild(createColumn);
        const column = document.getElementsByTagName("td")[numColmunn]; 
        column.textContent = i + "." + j;
        //adds the colour
        if((colour%2) === 0){
          column.style.backgroundColor = inputColour2;
        }
        else{
          column.style.backgroundColor = inputColour1;
        }
        colour++;
        numColmunn++;
      }
    }
  }

  function clicked(e){
    table.textContent = undefined;
    e.preventDefault();
    createTable(row.value, column.value, colour1.value, colour2.value);
  }


  btn.addEventListener("click", clicked);
}

// your functions